require 'rails_helper'

RSpec.describe CountryTargetGroupJoin, type: :model do
  describe 'validations' do
    let(:panel_provider) { create(:panel_provider) }
    let(:country) { Country.create(country_code: 'PL', panel_provider: panel_provider) }
    let!(:root_target_group) { TargetGroup.create(name: 'Europe', panel_provider: panel_provider) }

    it 'prevents associating Country with child TargetGroup' do
      child_target_group = TargetGroup.create(name: 'EU-5', panel_provider: panel_provider, parent: root_target_group)
      subject = described_class.new(country: country, target_group: child_target_group)
      expect(subject).not_to be_valid
      expect(subject.errors.details[:target_group][0]).to eq({ error: 'must be root' })
    end

    it 'allows associating Country with root TargetGroup' do
      subject = described_class.new(country: country, target_group: root_target_group)
      expect(subject).to be_valid
      subject.save
      expect(root_target_group.countries).to contain_exactly(country)
      expect(country.target_groups).to contain_exactly(root_target_group)
    end

    it 'checks presence of Country' do
      subject = described_class.new(target_group: root_target_group)
      expect(subject).not_to be_valid
      expect(subject.errors.details[:country][0]).to eq({ error: :blank })
    end

    it 'checks presence of TargetGroup' do
      subject = described_class.new(country: country)
      expect(subject).not_to be_valid
      expect(subject.errors.details[:target_group][0]).to eq({ error: :blank })
    end
  end
end
