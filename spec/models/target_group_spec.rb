require 'rails_helper'

RSpec.describe TargetGroup, type: :model do
  let(:panel_provider) { create(:panel_provider) }
  describe 'validations' do
    it 'is valid when name is set' do
      subject.assign_attributes({ name: 'Europe', panel_provider: panel_provider })
      expect(subject).to be_valid
    end

    it 'has errors when name is missing' do
      expect(subject).not_to be_valid
      expect(subject.errors.details[:name][0]).to eq({ error: :blank })
    end

    it 'has errors when name is too short' do
      subject.name = 'EU'
      expect(subject).not_to be_valid
      expect(subject.errors.details[:name][0]).to eq({ error: :too_short, count: 3 })
    end

    it 'has errors when name is too long' do
      subject.name = 'Chargoggagoggmanchauggagoggchaubunagungamaugg'
      expect(subject).not_to be_valid
      expect(subject.errors.details[:name][0]).to eq({ error: :too_long, count: 20 })
    end

    it 'checks name uniqueness' do
      other_targed_group = described_class.create(name: 'EU-1', panel_provider: panel_provider)
      subject.assign_attributes({ name: 'eu-1', panel_provider: panel_provider })
      expect { subject.save }.to raise_error(ActiveRecord::RecordNotUnique)
    end

    it 'checks panel_provider presence' do
      subject.name = 'Europe'
      expect(subject).not_to be_valid
      expect(subject.errors.details[:panel_provider][0]).to eq({ error: :blank })
    end
  end

  describe 'external_id and ancestry' do
    before do
      subject.assign_attributes({ name: 'Europe', panel_provider: panel_provider })
      subject.save
    end

    it 'gets external_id on save' do
      expect(subject).to be_persisted
      expect(subject.external_id.length).to eq(24)
    end

    it 'can form a tree' do
      expect(subject.is_root?).to be true
      first_lvl_children = []
      3.times do |i|
        child = described_class.create(name: "EU-#{i}", parent: subject, panel_provider: panel_provider)
        first_lvl_children << child
      end
      expect(subject.children.pluck(:id)).to contain_exactly(*first_lvl_children.map(&:id))

      first_lvl_child = first_lvl_children.sample
      second_lvl_child = described_class.create(name: "#{first_lvl_child.name}-1", parent: first_lvl_child, panel_provider: panel_provider)
      expect(second_lvl_child.parent).to eq(first_lvl_child)
      expect(second_lvl_child.root).to eq(subject)
      expect(second_lvl_child.depth).to eq(2)
    end
  end
end
