require 'rails_helper'

RSpec.describe Country, type: :model do
  describe 'validations' do
    let(:panel_provider) { create(:panel_provider) }

    it 'is valid when country_code and panel_provider is set' do
      subject.assign_attributes({ country_code: 'PL', panel_provider: panel_provider })
      expect(subject).to be_valid
    end

    it 'has errors when country_code is missing' do
      expect(subject).not_to be_valid
      expect(subject.errors.details[:country_code][0]).to eq({ error: :blank })
    end

    it 'has errors when country_code is too short' do
      subject.country_code = 'P'
      expect(subject).not_to be_valid
      expect(subject.errors.details[:country_code][0]).to eq({ error: :wrong_length, count: 2 })
    end

    it 'has errors when country_code is too long' do
      subject.country_code = 'POL'
      expect(subject).not_to be_valid
      expect(subject.errors.details[:country_code][0]).to eq({ error: :wrong_length, count: 2 })
    end

    it 'checks country_code uniqueness' do
      other_country = described_class.create(country_code: 'PL', panel_provider: panel_provider)
      subject.assign_attributes({ country_code: 'pl', panel_provider: panel_provider })
      expect { subject.save }.to raise_error(ActiveRecord::RecordNotUnique)
    end

    it 'checks panel_provider presence' do
      subject.country_code = 'PL'
      expect(subject).not_to be_valid
      expect(subject.errors.details[:panel_provider][0]).to eq({ error: :blank })
    end
  end
end
