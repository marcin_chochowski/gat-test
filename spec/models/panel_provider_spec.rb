require 'rails_helper'

RSpec.describe PanelProvider, type: :model do
  describe 'validations' do
    it 'is valid when code is set and of correct length' do
      subject.code = 'A1K122'
      expect(subject).to be_valid
    end

    it 'has errors when code is missing' do
      expect(subject).not_to be_valid
      expect(subject.errors.details[:code][0]).to eq({ error: :blank })
    end

    it 'has errors when code is too short' do
      subject.code = 'A1K12'
      expect(subject).not_to be_valid
      expect(subject.errors.details[:code][0]).to eq({ error: :wrong_length, count: 6 })
    end

    it 'has errors when code is too long' do
      subject.code = 'A1K1223'
      expect(subject).not_to be_valid
      expect(subject.errors.details[:code][0]).to eq({ error: :wrong_length, count: 6 })
    end
  end
end
