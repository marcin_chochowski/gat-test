require 'rails_helper'

RSpec.describe LocationGroup, type: :model do
  describe 'validations' do
    let(:country) { create(:country) }
    let(:panel_provider) { create(:panel_provider) }

    it 'is valid when name is set' do
      subject.assign_attributes({ name: 'Central Europe', panel_provider: panel_provider, country: country })
      expect(subject).to be_valid
    end

    it 'has errors when name is missing' do
      expect(subject).not_to be_valid
      expect(subject.errors.details[:name][0]).to eq({ error: :blank })
    end

    it 'has errors when name is too short' do
      subject.name = 'EU'
      expect(subject).not_to be_valid
      expect(subject.errors.details[:name][0]).to eq({ error: :too_short, count: 3 })
    end

    it 'has errors when name is too long' do
      subject.name = 'West part of Massachusetts'
      expect(subject).not_to be_valid
      expect(subject.errors.details[:name][0]).to eq({ error: :too_long, count: 20 })
    end

    it 'checks name uniqueness' do
      other_location = described_class.create(name: 'North Carolina', panel_provider: panel_provider, country: country)
      subject.assign_attributes({ name: 'North carolina', panel_provider: panel_provider, country: country })
      expect { subject.save }.to raise_error(ActiveRecord::RecordNotUnique)
    end

    it 'checks panel_provider presence' do
      subject.assign_attributes({ name: 'Central Europe', country: country })
      expect(subject).not_to be_valid
      expect(subject.errors.details[:panel_provider][0]).to eq({ error: :blank })
    end

    it 'checks country presence' do
      subject.assign_attributes({ name: 'Central Europe', panel_provider: panel_provider })
      expect(subject).not_to be_valid
      expect(subject.errors.details[:country][0]).to eq({ error: :blank })
    end
  end
end
