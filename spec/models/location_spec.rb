require 'rails_helper'

RSpec.describe Location, type: :model do
  describe 'validations' do
    it 'is valid when name is set' do
      subject.name = 'Wroclaw'
      expect(subject).to be_valid
    end

    it 'has errors when name is missing' do
      expect(subject).not_to be_valid
      expect(subject.errors.details[:name][0]).to eq({ error: :blank })
    end

    it 'has errors when name is too short' do
      subject.name = 'LA'
      expect(subject).not_to be_valid
      expect(subject.errors.details[:name][0]).to eq({ error: :too_short, count: 3 })
    end

    it 'has errors when name is too long' do
      subject.name = 'Chargoggagoggmanchauggagoggchaubunagungamaugg'
      expect(subject).not_to be_valid
      expect(subject.errors.details[:name][0]).to eq({ error: :too_long, count: 20 })
    end

    it 'checks name uniqueness' do
      other_location = described_class.create(name: 'New York')
      subject.name = 'new York'
      expect { subject.save }.to raise_error(ActiveRecord::RecordNotUnique)
    end
  end

  it 'gets external_id on save' do
    subject.name = 'Wroclaw'
    subject.save
    expect(subject).to be_persisted
    expect(subject.external_id.length).to eq(24)
  end
end
