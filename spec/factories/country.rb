FactoryBot.define do
  factory :country do
    country_code 'PL'
    panel_provider { create(:panel_provider) }
  end
end
