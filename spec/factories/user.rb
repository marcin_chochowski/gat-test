FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "john#{n}@example.com" }
    password "secret"
  end
end
