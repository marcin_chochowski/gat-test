require 'rails_helper'

RSpec.describe LongArraysCounterService do
  let(:array1) { [1, 2, 3, 4, 5] }
  let(:array2) { [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11] }
  let(:array3) { [1, 2, 3, 4, 5] }
  let(:array4) { [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11] }
  let(:array5) { [1, 2, 3, 4, 5] }

  let(:hash) { {
    a: array1,
    b: 'gyuguyf',
    c: 'dwqdqwdq',
    d: {
      ddq: 'dasdasd',
      dfv: array2,
      vfv: {
        dwedw: array3,
        fsdfs: 'dadwqf'
      },
      ewferf: array4
    },
    e: array5
  } }

  subject { described_class.new(hash: hash) }

  describe 'run' do
    it 'counts length of each array in the hash' do
      expect(array1).to receive(:length).and_return(5)
      expect(array2).to receive(:length).and_return(11)
      expect(array3).to receive(:length).and_return(5)
      expect(array4).to receive(:length).and_return(11)
      expect(array5).to receive(:length).and_return(5)
      subject.run
    end

    it 'computes numer of arrays longer than 10' do
      subject.run
      expect(subject.count).to eq(2)
    end
  end
end
