require 'rails_helper'

RSpec.describe TargetGroupLoaderService do
  subject { described_class.new(country_code: 'ja') }

  describe 'load_target_groups' do
    it 'fetches locations for known country' do
      country = double('country')
      panel_provider = double('panel_provider')
      target_groups = double('target_groups')
      expect(Country).to receive(:find_by!).with(country_code: 'ja').and_return(country)
      expect(country).to receive(:panel_provider).and_return(panel_provider)
      expect(panel_provider).to receive(:target_groups).and_return(target_groups)
      expect(subject.load_target_groups).to eq(target_groups)
    end

    it 'raises error for unknown country' do
      expect(Country).to receive(:find_by!).with(country_code: 'ja').and_raise(ActiveRecord::RecordNotFound)
      expect { subject.load_target_groups }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
