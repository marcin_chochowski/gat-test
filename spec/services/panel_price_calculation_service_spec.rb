require 'rails_helper'

RSpec.describe PanelPriceCalculationService do
  let!(:panel_provider1) { create(:panel_provider) }
  let!(:panel_provider2) { create(:panel_provider) }
  let!(:panel_provider3) { create(:panel_provider) }

  describe 'calculate_price' do
    context 'first provider' do
      subject { described_class.new(provider: panel_provider1) }

      it 'calls correct calculation method' do
        expect(subject).to receive(:a_letter_based_price)
        subject.calculate_price
      end
    end

    context 'second provider' do
      subject { described_class.new(provider: panel_provider2) }

      it 'calls correct calculation method' do
        expect(subject).to receive(:arrays_based_price)
        subject.calculate_price
      end
    end

    context 'third provider' do
      subject { described_class.new(provider: panel_provider3) }

      it 'calls correct calculation method' do
        expect(subject).to receive(:html_nodes_based_price)
        subject.calculate_price
      end
    end
  end

  describe 'a_letter_based_price' do
    subject { described_class.new(provider: panel_provider1) }

    it 'counts number of a letters in page content and divides by 100' do
      page = double('PageOpenerService')
      expect(PageOpenerService).to receive(:new).with(url: 'http://time.com/').and_return(page)
      expect(page).to receive(:open_page)
      expect(page).to receive(:text).and_return('this page contains 5 "a" characters')
      expect(subject.calculate_price).to eq(5/100.0)
    end
  end

  describe 'arrays_based_price' do
    subject { described_class.new(provider: panel_provider2) }
    let(:hash) { JSON.load(File.read('spec/files/lotr.json')) }
    it 'counts number of a letters in page content and divides by 100' do
      json_loader = double('JsonLoaderService')
      expect(JsonLoaderService).to receive(:new).
        with(url: "http://openlibrary.org/search.json?q=the+lord+of+the+rings").and_return(json_loader)
      expect(json_loader).to receive(:load_json)
      expect(json_loader).to receive(:json).and_return(hash)
      expect(subject.calculate_price).to eq(2.0)
    end
  end

  describe 'html_nodes_based_price' do
    subject { described_class.new(provider: panel_provider3) }

    it 'counts html nodes of a page and divides by 100' do
      html = Nokogiri::HTML('<html><body><div><ul><li>1</li><li>2</li><li>3</li></ul></div></body></html>')
      page = double('PageOpenerService', page: html)
      expect(PageOpenerService).to receive(:new).with(url: 'http://time.com/').and_return(page)
      expect(page).to receive(:open_page)
      expect(page).to receive(:traverse).
        and_yield(html).
        and_yield(html.elements.children.first).
        and_yield(html.elements.children.first.children.first).
        and_yield(html.elements.children.first.children.first.children.first).
        and_yield(html.elements.children.first.children.first.children.first.children[0]).
        and_yield(html.elements.children.first.children.first.children.first.children[1]).
        and_yield(html.elements.children.first.children.first.children.first.children[2])
      expect(subject.calculate_price).to eq(7/100.0)
    end
  end
end
