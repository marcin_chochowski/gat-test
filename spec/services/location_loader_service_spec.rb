require 'rails_helper'

RSpec.describe LocationLoaderService do
  subject { described_class.new(country_code: 'ja') }

  describe 'load_locations' do
    it 'fetches locations for known country' do
      country = double('country')
      locations = double('locations')
      panel_provider = double('panel_provider')
      location_groups = double('location_groups')
      expect(Country).to receive(:find_by!).with(country_code: 'ja').and_return(country)
      expect(Location).to receive(:joins).with(:location_groups).and_return(locations)
      expect(country).to receive(:panel_provider).and_return(panel_provider)
      expect(panel_provider).to receive(:location_groups).and_return(location_groups)
      expect(locations).to receive(:where).with(location_groups: { id: location_groups }).and_return(locations)
      expect(locations).to receive(:uniq).and_return(locations)
      expect(subject.load_locations).to eq(locations)
    end

    it 'raises error for unknown country' do
      expect(Country).to receive(:find_by!).with(country_code: 'ja').and_raise(ActiveRecord::RecordNotFound)
      expect { subject.load_locations }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
