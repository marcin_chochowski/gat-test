require 'rails_helper'

RSpec.describe Api::Private::V1::TargetGroupsController, type: :request do
  describe 'index' do
    let(:user) { create(:user) }
    let(:token) { Knock::AuthToken.new(payload: { sub: user.id }).token }

    let(:panel_provider1) { create(:panel_provider) }
    let(:panel_provider2) { create(:panel_provider) }
    let!(:great_britain) { create(:country, country_code: 'GB', panel_provider: panel_provider1) }
    let!(:germany) { create(:country, country_code: 'DE', panel_provider: panel_provider2) }
    let!(:other_country) { create(:country) }

    let(:north_gb_group1) { TargetGroup.create(name: 'NorthGB', panel_provider: panel_provider1) }
    let(:north_gb_group1_1) { TargetGroup.create(name: 'NorthGB-1', panel_provider: panel_provider1, parent: north_gb_group1) }
    let(:north_gb_group1_2) { TargetGroup.create(name: 'NorthGB-2', panel_provider: panel_provider1, parent: north_gb_group1) }
    let(:north_gb_group2) { TargetGroup.create(name: 'SouthGB', panel_provider: panel_provider1) }
    let(:south_de_group) { TargetGroup.create(name: 'SouthDE', panel_provider: panel_provider2, secret_code: 'Bazinga') }

    it 'returns all target_groups in Great Britain' do
      expected_groups = [north_gb_group1, north_gb_group1_1, north_gb_group1_2, north_gb_group2].map(&:external_id)
      unexpected_groups = [south_de_group.external_id]
      get_with_token '/api/private/v1/target-groups?filter[country]=gb', token
      expect(json['data']).to contain_exactly_jsonapi_resources('target-groups', expected_groups)
    end

    it 'returns the target_group in Germany' do
      expected_groups = [south_de_group.external_id]
      unexpected_groups = [north_gb_group1, north_gb_group1_1, north_gb_group1_2, north_gb_group2].map(&:external_id)
      get_with_token '/api/private/v1/target-groups?filter[country]=de', token
      expect(json['data']).to contain_exactly_jsonapi_resources('target-groups', expected_groups)
      expect(json['data'][0]['attributes']['name']).to eq('SouthDE')
      expect(json['data'][0]['attributes']['secret-code']).to eq('Bazinga')
    end

    context 'no results' do
      before do
        unexpected_groups = [north_gb_group1, north_gb_group1_1, north_gb_group1_2, north_gb_group2, south_de_group].map(&:external_id)
      end
      it 'returns no target_group for the other_country' do
        get_with_token "/api/private/v1/target-groups?filter[country]=#{other_country.country_code}", token
        expect(json['data']).to be_empty
      end

      it 'is not_found for unknown country' do
        get_with_token '/api/private/v1/target-groups?filter[country]=ja', token
        expect(response).to be_not_found
        expect(json['errors'][0]['title']).to eq('Not found')
      end

      it 'is not_found country filter missing' do
        get_with_token '/api/private/v1/target-groups', token
        expect(response).to be_not_found
        expect(json['errors'][0]['title']).to eq('Not found')
      end
    end

    context 'without token' do
      it 'is unauthorized' do
        get '/api/private/v1/target-groups?filter[country]=de'
        expect(response).to be_unauthorized
      end
    end
  end
end
