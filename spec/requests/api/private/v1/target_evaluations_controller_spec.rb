require 'rails_helper'

RSpec.describe Api::Private::V1::TargetEvaluationsController, type: :request do
  describe 'create' do
    let(:user) { create(:user) }
    let(:token) { Knock::AuthToken.new(payload: { sub: user.id }).token }
    let(:panel_provider) { create(:panel_provider) }
    let(:great_britain) { create(:country, country_code: 'GB', panel_provider: panel_provider) }
    let(:germany) { create(:country, country_code: 'DE') }
    let(:poland) { create(:country) }
    let(:north_gb_location_group) {
      LocationGroup.create(name: 'NorthGB', country: great_britain, panel_provider: panel_provider)
    }
    let(:glasgow) { Location.create(name: 'Glasgow', location_groups: [north_gb_location_group]) }
    let(:edyngurg) { Location.create(name: 'Edyngurg', location_groups: [north_gb_location_group]) }
    let(:north_gb_target_group) { TargetGroup.create(name: 'NorthGB', panel_provider: panel_provider) }

    let(:evaluation_params) { {
      data: {
        type: 'target-evaluations',
        attributes: {
          'country-code': 'gb',
          'target-group-id': north_gb_target_group.external_id,
          'locations': [{
            'id': glasgow.external_id,
            'panel-size': 200
          }]
        }
      }
    } }
    describe 'parameter presence requirements' do
      it 'expects target-group-id' do
        evaluation_params[:data][:attributes].delete(:'target-group-id')
        post_with_token '/api/private/v1/target-evaluations', token, params: evaluation_params
        expect(response).to be_bad_request
        expect(json['errors'][0]['title']).to eq('Missing Parameter')
        expect(json['errors'][0]['detail']).to eq('The required parameter, target-group-id, is missing.')
      end

      it 'expects country-code' do
        evaluation_params[:data][:attributes].delete(:'country-code')
        post_with_token '/api/private/v1/target-evaluations', token, params: evaluation_params
        expect(response).to be_bad_request
        expect(json['errors'][0]['title']).to eq('Missing Parameter')
        expect(json['errors'][0]['detail']).to eq('The required parameter, country-code, is missing.')
      end

      it 'expects locations' do
        evaluation_params[:data][:attributes].delete(:'locations')
        post_with_token '/api/private/v1/target-evaluations', token, params: evaluation_params
        expect(response).to be_bad_request
        expect(json['errors'][0]['title']).to eq('Missing Parameter')
        expect(json['errors'][0]['detail']).to eq('The required parameter, locations, is missing.')
      end

      it 'expects id of each location' do
        evaluation_params[:data][:attributes][:'locations'] << { 'panel-size': 123 }
        post_with_token '/api/private/v1/target-evaluations', token, params: evaluation_params
        expect(response).to be_bad_request
        expect(json['errors'][0]['title']).to eq('Missing Parameter')
        expect(json['errors'][0]['detail']).to eq('The required parameter, id, is missing.')
      end

      it 'expects panel-size of each location' do
        evaluation_params[:data][:attributes][:'locations'] << { 'id': edyngurg.external_id }
        post_with_token '/api/private/v1/target-evaluations', token, params: evaluation_params
        expect(response).to be_bad_request
        expect(json['errors'][0]['title']).to eq('Missing Parameter')
        expect(json['errors'][0]['detail']).to eq('The required parameter, panel-size, is missing.')
      end
    end

    describe 'parameter validity' do
      it 'expects id of existing TargetGroup' do
        evaluation_params[:data][:attributes][:'target-group-id'] = 'invalidID'
        post_with_token '/api/private/v1/target-evaluations', token, params: evaluation_params
        expect(response).to be_not_found
        expect(json['errors'][0]['title']).to eq('Not found')
        expect(json['errors'][0]['detail']).to eq("Couldn't find TargetGroup")
      end

      it 'expects code of existing Country' do
        evaluation_params[:data][:attributes][:'country-code'] = 'BB'
        post_with_token '/api/private/v1/target-evaluations', token, params: evaluation_params
        expect(response).to be_not_found
        expect(json['errors'][0]['title']).to eq('Not found')
        expect(json['errors'][0]['detail']).to eq("Couldn't find Country")
      end

      it 'expects valid id of each location' do
        evaluation_params[:data][:attributes][:'locations'] << { 'id': 'invalidID', 'panel-size': 123 }
        post_with_token '/api/private/v1/target-evaluations', token, params: evaluation_params
        expect(response).to be_not_found
        expect(json['errors'][0]['title']).to eq('Not found')
        expect(json['errors'][0]['detail']).to eq("Couldn't find Location")
      end

      it 'expects valid panel-size of each location' do
        evaluation_params[:data][:attributes][:'locations'] << { 'id': 'invalidID', 'panel-size': 1223 }
        post_with_token '/api/private/v1/target-evaluations', token, params: evaluation_params
        expect(response).to be_unprocessable
        expect(json['errors'][0]['title']).to eq('Invalid parameter')
        expect(json['errors'][0]['detail']).to eq('panel-size should be between 10 and 1000')
      end
    end

    describe 'price calculation' do
      it 'calculates price based on panel_provider' do
        ppc_service = double('PanelPriceCalculationService')
        expect(PanelPriceCalculationService).to receive(:new).with(provider: panel_provider).and_return(ppc_service)
        expect(ppc_service).to receive(:calculate_price).and_return(123.91)
        post_with_token '/api/private/v1/target-evaluations', token, params: evaluation_params
        expect(response).to be_created
        expect(json['data']['type']).to eq('target-evaluations')
        expect(json['data']['attributes']['price']).to eq(123.91)
      end
    end
  end
end
