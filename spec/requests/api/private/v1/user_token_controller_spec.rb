require 'rails_helper'

RSpec.describe Api::Private::V1::UserTokenController, type: :request do
  describe 'create' do
    let(:user) { create(:user) }

    context 'missing auth params' do
      it 'fails' do
        post '/api/private/v1/user_token'
        expect(response).to be_bad_request
        expect(json['errors'][0]['title']).to eq('Missing Parameter')
      end
    end

    context 'correct credentials' do
      let(:auth_params) { {
        data: {
          type: 'user_tokens',
          attributes: {
            email: user.email,
            password: 'secret'
          }
        }
      } }

      it 'returns token for the user' do
        Timecop.freeze(Time.now) do
          token = Knock::AuthToken.new(payload: { sub: user.id }).token
          post '/api/private/v1/user_token', params: auth_params
          expect(response).to be_created
          expect(json['data']['type']).to eq('user_tokens')
          expect(json['data']['id']).to eq(token)
        end
      end
    end

    context 'incorrect credentials' do
      let(:auth_params) { {
        data: {
          type: 'user_tokens',
          attributes: {
            email: user.email,
            password: 'wrong'
          }
        }
      } }

      it 'responds with not_found' do
        post '/api/private/v1/user_token', params: auth_params
        expect(response).to be_not_found
        expect(json).to be_nil
      end
    end
  end
end
