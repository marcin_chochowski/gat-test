require 'rails_helper'

RSpec.describe Api::Public::V1::LocationsController, type: :request do
  describe 'index' do
    let(:panel_provider1) { create(:panel_provider) }
    let(:panel_provider2) { create(:panel_provider) }
    let(:great_britain) { create(:country, country_code: 'GB', panel_provider: panel_provider1) }
    let(:germany) { create(:country, country_code: 'DE', panel_provider: panel_provider2) }
    let(:other_country) { create(:country) }

    let(:north_gb_group1) { LocationGroup.create(name: 'NorthGB', country: great_britain, panel_provider: panel_provider1) }
    let(:north_gb_group2) { LocationGroup.create(name: 'SouthGB', country: great_britain, panel_provider: panel_provider1) }
    let(:south_de_group) { LocationGroup.create(name: 'SouthDE', country: germany, panel_provider: panel_provider2) }

    let(:glasgow) { Location.create(name: 'Glasgow', location_groups: [north_gb_group1]) }
    let(:edyngurg) { Location.create(name: 'Edyngurg', location_groups: [north_gb_group1]) }
    let(:leeds) { Location.create(name: 'Leeds', location_groups: [north_gb_group1, north_gb_group2]) }
    let(:london) { Location.create(name: 'London', location_groups: [north_gb_group2]) }
    let(:oksford) { Location.create(name: 'Oksford', location_groups: [north_gb_group2]) }
    let(:bristol) { Location.create(name: 'Bristol', location_groups: [north_gb_group2]) }

    let(:munich) { Location.create(name: 'Munich', location_groups: [south_de_group]) }

    it 'returns all locations in Great Britain' do
      expected_cities = [glasgow, edyngurg, leeds, london, oksford, bristol].map(&:external_id)
      unexpected_cities = [munich.external_id]
      get '/api/public/v1/locations?filter[country]=gb'
      expect(json['data']).to contain_exactly_jsonapi_resources('locations', expected_cities)
    end

    it 'returns the location in Germany' do
      unexpected_cities = [glasgow, edyngurg, leeds, london, oksford, bristol].map(&:external_id)
      expected_cities = [munich.external_id]
      get '/api/public/v1/locations?filter[country]=de'
      expect(json['data']).to contain_exactly_jsonapi_resources('locations', expected_cities)
    end

    context 'no results' do
      before do
        unexpected_cities = [glasgow, edyngurg, leeds, london, oksford, bristol, munich].map(&:external_id)
      end
      it 'returns no location for the other_country' do
        get "/api/public/v1/locations?filter[country]=#{other_country.country_code}"
        expect(json['data']).to be_empty
      end

      it 'is not_found for unknown country' do
        get '/api/public/v1/locations?filter[country]=ja'
        expect(response).to be_not_found
        expect(json['errors'][0]['title']).to eq('Not found')
      end

      it 'is not_found country filter missing' do
        get '/api/public/v1/locations'
        expect(response).to be_not_found
        expect(json['errors'][0]['title']).to eq('Not found')
      end
    end
  end
end
