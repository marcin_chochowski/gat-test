class JsonLoaderService
  require 'open-uri'

  attr_accessor :json

  def initialize(params = {})
    @url = params[:url]
    @json = nil
  end

  def load_json
    @json = JSON.load(open(@url))
  end
end
