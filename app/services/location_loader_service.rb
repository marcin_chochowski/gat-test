class LocationLoaderService

  def initialize(params={})
    @country_code = params[:country_code]
  end

  def load_locations
    Location.
      joins(:location_groups).
      where(location_groups: { id: country.panel_provider.location_groups }).
      uniq
  end

  private

  def country
    @country ||= Country.find_by!(country_code: @country_code)
  end
end
