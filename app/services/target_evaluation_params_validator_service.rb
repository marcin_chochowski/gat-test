class TargetEvaluationParamsValidatorService
  MAX_PANEL_SIZE = 1000
  MIN_PANEL_SIZE = 10

  attr_accessor :country

  def initialize(options={})
    @params = options[:params]
  end

  def validate
    target_group = TargetGroup.find_by!(external_id: @params['target-group-id'])
    @country = Country.find_by!(country_code: @params['country-code'])
    locations = @params['locations'].map do |location_params|
      unless location_params['panel-size'].to_i.between?(MIN_PANEL_SIZE, MAX_PANEL_SIZE)
        raise ::Exceptions::WrongPanelSize.new("panel-size should be between #{MIN_PANEL_SIZE} and #{MAX_PANEL_SIZE}")
      end
      Location.find_by!(external_id: location_params['id'])
    end
  end
end
