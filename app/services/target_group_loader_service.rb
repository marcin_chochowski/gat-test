class TargetGroupLoaderService

  def initialize(params={})
    @country_code = params[:country_code]
  end

  def load_target_groups
    country.panel_provider.target_groups
  end

  private

  def country
    @country ||= Country.find_by!(country_code: @country_code)
  end
end
