class PanelPriceCalculationService
  def initialize(params = {})
    @panel_provider = params[:provider]
  end

  def calculate_price
    all_panel_provider_ids = PanelProvider.pluck(:id)
    case all_panel_provider_ids.index(@panel_provider.id)
    when 0
      a_letter_based_price
    when 1
      arrays_based_price
    when 2
      html_nodes_based_price
    end
  end

  private

  def a_letter_based_price
    page = PageOpenerService.new(url: 'http://time.com/')
    page.open_page
    page.text.scan(/a/).length / 100.0
  end

  def arrays_based_price
    json_loader = JsonLoaderService.new(url: "http://openlibrary.org/search.json?q=the+lord+of+the+rings")
    json_loader.load_json
    lac = LongArraysCounterService.new(hash: json_loader.json)
    lac.run
    lac.count.to_f
  end

  def html_nodes_based_price
    page = PageOpenerService.new(url: 'http://time.com/')
    page.open_page
    count = 0
    page.traverse { |node| count += 1 }
    count / 100.0
  end
end
