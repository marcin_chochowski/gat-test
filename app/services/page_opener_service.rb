class PageOpenerService
  require 'open-uri'

  def initialize(params = {})
    @url = params[:url]
    @page = nil
  end

  def open_page
    @page = Nokogiri::HTML(open(@url))
  end

  def text
    @page.text
  end

  def traverse(&block)
    @page.traverse(&block)
  end
end
