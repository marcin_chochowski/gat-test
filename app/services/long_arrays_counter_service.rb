class LongArraysCounterService

  attr_accessor :count

  def initialize(params)
    @count = 0
    @hash = params[:hash]
  end

  def run(enumerable = @hash)
    enumerable.each do |k, v|
      # if enumerable is an array then k is the value
      # if enumerable is a hash then v is the value
      value = v || k

      if value.is_a?(Hash)
        run(value)
      elsif value.is_a?(Array)
        run(value)
        if value.length > 10
          @count += 1
        end
      end
    end
  end
end
