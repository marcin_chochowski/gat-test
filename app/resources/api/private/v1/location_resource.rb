class Api::Private::V1::LocationResource < JSONAPI::Resource
  attributes :name

  custom_filter :country

  def id
    @model.external_id
  end
end
