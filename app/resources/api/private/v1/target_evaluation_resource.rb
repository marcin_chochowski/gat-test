class Api::Private::V1::TargetEvaluationResource < JSONAPI::Resource
  attributes :country_code, :target_group_id, :locations, :price

  def id
    @model.external_id
  end
end
