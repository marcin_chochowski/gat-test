class Api::Private::V1::TargetGroupResource < JSONAPI::Resource
  attributes :name, :secret_code

  custom_filter :country

  def id
    @model.external_id
  end
end
