class Api::Public::V1::TargetGroupResource < JSONAPI::Resource
  attributes :name

  custom_filter :country

  def id
    @model.external_id
  end
end
