class PanelProvider < ApplicationRecord
  has_many :location_groups
  has_many :target_groups
  has_many :countries

  validates :code, presence: true, length: { is: 6 }
end
