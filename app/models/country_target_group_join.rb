class CountryTargetGroupJoin < ApplicationRecord
  belongs_to :country
  belongs_to :target_group

  validates :country, presence: true
  validates :target_group, presence: true
  validate :target_group_must_be_root

  def target_group_must_be_root
    errors.add(:target_group, 'must be root') unless target_group&.is_root?
  end
end
