class LocationGroup < ApplicationRecord
  belongs_to :panel_provider
  belongs_to :country
  has_and_belongs_to_many :locations

  validates :panel_provider, presence: true
  validates :country, presence: true
  validates :name, presence: true, length: { in: 3..20 }
end
