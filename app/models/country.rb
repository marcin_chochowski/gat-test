class Country < ApplicationRecord
  belongs_to :panel_provider
  has_many :location_groups
  has_many :country_target_group_joins
  has_many :target_groups, through: :country_target_group_joins

  validates :panel_provider, presence: true
  validates :country_code, presence: true, length: { is: 2 }
end
