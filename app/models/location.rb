class Location < ApplicationRecord
  has_secure_token :external_id

  has_and_belongs_to_many :location_groups

  validates :name, presence: true, length: { in: 3..20 }
end
