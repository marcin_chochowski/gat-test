class TargetGroup < ApplicationRecord
  has_secure_token :external_id
  has_ancestry

  belongs_to :panel_provider
  has_many :country_target_group_joins
  has_many :countries, through: :country_target_group_joins

  validates :panel_provider, presence: true
  validates :name, presence: true, length: { in: 3..20 }
end
