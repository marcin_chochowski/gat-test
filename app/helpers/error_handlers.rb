module ErrorHandlers
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound, with: :on_record_not_found
    unless Rails.env.development?
      rescue_from ActionController::RoutingError, with: :on_record_not_found
    end
    rescue_from Exceptions::WrongPanelSize, with: :on_invalid_parameter
  end

  private

  def on_record_not_found(exception)
    jsonapi_render_errors json: [{ title: 'Not found', detail: exception.message }], status: :not_found
  end

  def on_invalid_parameter(exception)
    jsonapi_render_errors json: [{ title: 'Invalid parameter', detail: exception.message }], status: :unprocessable_entity
  end
end
