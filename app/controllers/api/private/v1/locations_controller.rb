class Api::Private::V1::LocationsController < Api::Private::V1::BaseController

  def index
    @locations = LocationLoaderService.new(country_code: params.dig('filter', 'country')).load_locations
    jsonapi_render json: @locations
  end
end
