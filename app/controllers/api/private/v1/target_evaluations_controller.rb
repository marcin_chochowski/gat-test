class Api::Private::V1::TargetEvaluationsController < Api::Private::V1::BaseController
  before_action :evaluation_params, only: [:create]
  before_action :validate_params, only: [:create]

  def create
    price_calculator = PanelPriceCalculationService.new(provider: @country.panel_provider)
    @target_evaluation = TargetEvaluation.create(resource_params.merge(price: price_calculator.calculate_price))
    jsonapi_render json: @target_evaluation, status: :created
  end

  private

  def evaluation_params
    @evaluation_params ||= params.require(:data).require(:attributes).tap do |attributes|
      attributes.require('country-code')
      attributes.require('target-group-id')
      attributes.require('locations')
      attributes['locations'].each { |a| a.require('id') }
      attributes['locations'].each { |a| a.require('panel-size') }
    end.permit('country-code', 'target-group-id', locations: [:id, 'panel-size'])
  rescue ActionController::ParameterMissing => e
    jsonapi_render_errors JSONAPI::Exceptions::ParameterMissing.new(e.param).errors
  end

  def validate_params
    params_validator = TargetEvaluationParamsValidatorService.new(params: evaluation_params)
    params_validator.validate
    @country = params_validator.country
  end
end
