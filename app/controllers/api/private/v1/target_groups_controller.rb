class Api::Private::V1::TargetGroupsController < Api::Private::V1::BaseController

  def index
    @target_groups = TargetGroupLoaderService.new(country_code: params.dig('filter', 'country')).load_target_groups
    jsonapi_render json: @target_groups
  end
end
