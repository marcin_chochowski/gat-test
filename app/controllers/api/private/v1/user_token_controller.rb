class Api::Private::V1::UserTokenController < Knock::AuthTokenController
  rescue_from ActionController::ParameterMissing, with: :parameter_not_found

  def create
    render json: {
      data: {
        type: 'user_tokens',
        id: auth_token.token
      }
    }, status: :created
  end

  private

  def auth_params
    params.require(:data).require(:attributes).permit(:email, :password)
  end

  def parameter_not_found(exception)
    render json: { errors: [{ title: 'Missing Parameter', detail: exception.message }] }, status: :bad_request
  end
end
