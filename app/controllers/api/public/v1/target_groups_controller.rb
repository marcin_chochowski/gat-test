class Api::Public::V1::TargetGroupsController < ApplicationController

  def index
    @target_groups = TargetGroupLoaderService.new(country_code: params.dig('filter', 'country')).load_target_groups
    jsonapi_render json: @target_groups
  end
end
