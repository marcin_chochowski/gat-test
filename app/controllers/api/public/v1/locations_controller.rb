class Api::Public::V1::LocationsController < ApplicationController

  def index
    @locations = LocationLoaderService.new(country_code: params.dig('filter', 'country')).load_locations
    jsonapi_render json: @locations
  end
end
