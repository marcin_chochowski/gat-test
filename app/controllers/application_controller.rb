class ApplicationController < ActionController::API
  include Knock::Authenticable
  include JSONAPI::Utils
  include ErrorHandlers
end
