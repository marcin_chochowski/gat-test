Rails.application.routes.draw do
  namespace :api do
    namespace :public do
      namespace :v1 do
        jsonapi_resources 'locations', only: [:index]
        jsonapi_resources 'target_groups', only: [:index]
      end
    end

    namespace :private do
      namespace :v1 do
        post 'user_token' => 'user_token#create'
        jsonapi_resources 'locations', only: [:index]
        jsonapi_resources 'target_groups', only: [:index]
        jsonapi_resources 'target_evaluations', only: [:create]
      end
    end
  end
end
