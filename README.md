# GAT - test app

Install Ruby 2.5 then run:

```
gem install bundler
bundle
rails db:migrate
rails db:seed
rails s
```

### Public API Endpoints

```
curl -X GET \
  'http://localhost:3000/api/public/v1/locations?filter[country]=gb' \
```

--------------------

```
curl -X GET \
  'http://localhost:3000/api/public/v1/target-groups?filter[country]=us' \
```

### Authorization

Send:

```
curl -X POST \
  http://localhost:3000/api/private/v1/user_token \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F 'data[attributes][email]=test@example.com' \
  -F 'data[attributes][password]=secret'
```

Response will contain `id`, take it as your token and send it in `Authorization` header with each request to Private API (see examples below).

### Private API Endpoints

```
curl -X GET \
  'http://localhost:3000/api/private/v1/locations?filter[country]=pl' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MTg5OTA2MzQsInN1YiI6MX0.oQmqWgZG1hBVXwGBSqBw1CqGULSRB0ptq6scmS2w4gM' \
```

--------------------


```
curl -X GET \
  'http://localhost:3000/api/private/v1/target-groups??filter[country]=gb' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MTg5OTA2MzQsInN1YiI6MX0.oQmqWgZG1hBVXwGBSqBw1CqGULSRB0ptq6scmS2w4gM' \
```

--------------------

```
curl -X POST \
  http://localhost:3000/api/private/v1/target-evaluations \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MTg5OTA2MzQsInN1YiI6MX0.oQmqWgZG1hBVXwGBSqBw1CqGULSRB0ptq6scmS2w4gM' \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F 'data[type]=target-evaluations' \
  -F 'data[attributes][target-group-id]=Yxf1rEGP9BWpxKJMUJGTM96w' \
  -F 'data[attributes][country-code]=us' \
  -F 'data[attributes][locations][][id]=uyAzxodUK4sDYb2xQ32mJxmH' \
  -F 'data[attributes][locations][][panel-size]=200'
```
