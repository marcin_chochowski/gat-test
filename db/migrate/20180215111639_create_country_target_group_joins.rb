class CreateCountryTargetGroupJoins < ActiveRecord::Migration[5.1]
  def change
    create_table :country_target_group_joins do |t|
      t.references :country, foreign_key: true
      t.references :target_group, foreign_key: true
    end
  end
end
