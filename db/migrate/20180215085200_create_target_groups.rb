class CreateTargetGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :target_groups do |t|
      t.citext :name, null: false
      t.string :external_id, null: false
      t.string :secret_code
      t.references :panel_provider, null: false

      t.timestamps
    end
    add_index :target_groups, [:name], unique: true
    add_index :target_groups, [:external_id], unique: true
  end
end
