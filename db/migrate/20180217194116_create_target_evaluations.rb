class CreateTargetEvaluations < ActiveRecord::Migration[5.1]
  def change
    create_table :target_evaluations do |t|
      t.float :price
      t.string :external_id, null: false, index: true
      t.string :country_code, null: false
      t.string :target_group_id, null: false
      t.text :locations, array: true

      t.timestamps
    end
  end
end
