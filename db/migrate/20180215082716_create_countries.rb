class CreateCountries < ActiveRecord::Migration[5.1]
  def change
    create_table :countries do |t|
      t.citext :country_code, null: false
      t.references :panel_provider, null: false

      t.timestamps
    end
    add_index :countries, [:country_code], unique: true
  end
end
