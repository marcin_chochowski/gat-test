class CreateLocations < ActiveRecord::Migration[5.1]
  def change
    enable_extension 'citext'

    create_table :locations do |t|
      t.citext :name, null: false
      t.string :external_id, null: false
      t.string :secret_code

      t.timestamps
    end
    add_index :locations, [:name], unique: true
    add_index :locations, [:external_id], unique: true
  end
end
