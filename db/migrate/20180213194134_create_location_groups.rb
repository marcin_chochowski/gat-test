class CreateLocationGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :location_groups do |t|
      t.citext :name, null: false
      t.references :panel_provider, null: false
      t.references :country, null: false

      t.timestamps
    end
    add_index :location_groups, [:name], unique: true
    add_index :location_groups, [:panel_provider_id, :country_id]
  end
end
