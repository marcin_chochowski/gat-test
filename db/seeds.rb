User.destroy_all
Country.destroy_all
Location.destroy_all
LocationGroup.destroy_all
TargetGroup.destroy_all
PanelProvider.destroy_all

User.create(email: 'test@example.com', password: 'secret')

country_codes = ['PL', 'GB', 'US']
3.times do |i|
  panel_provider = PanelProvider.create(code: "AAA00#{i}")
  Country.create(country_code: country_codes[i], panel_provider: panel_provider)
end

Country.all.each do |country|
  LocationGroup.create(name: "#{country.country_code}0", country: country, panel_provider: country.panel_provider)
  TargetGroup.create(name: "#{country.country_code}0", panel_provider: country.panel_provider)
end

some_country = Country.all.sample
LocationGroup.create(
  name: "#{some_country.country_code}x", country: some_country, panel_provider: some_country.panel_provider
)
TargetGroup.create(name: "#{some_country.country_code}x", panel_provider: some_country.panel_provider)

def pl_cities
  ['Wroclaw', 'Warsaw', 'Krakow', 'Lodz', 'Gdansk', 'Katowice']
end

def gb_cities
  ['London', 'Manchester', 'Edynburg', 'Bristol', 'Liverpool', 'Cambrigde', 'Birmingham']
end

def us_cities
 ['New York', 'Washington', 'San Francisco', 'Chicago', 'Boston', 'Los Angeles', 'Houston']
end

Country.all.each do |country|
  cities = send("#{country.country_code.downcase}_cities")
  cities.each do |city|
    Location.create(name: city, location_groups: [country.location_groups.sample])
  end
end

root_groups = TargetGroup.all

root_groups.each do |root_group|
  some_country = Country.all.sample
  parent = root_group
  3.times do |i|
    root_group = TargetGroup.create(
      name: "#{root_group.name}-#{i}", panel_provider: some_country.panel_provider, parent: root_group
    )
  end
end
