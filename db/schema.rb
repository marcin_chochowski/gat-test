# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180217194116) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "citext"

  create_table "countries", force: :cascade do |t|
    t.citext "country_code", null: false
    t.bigint "panel_provider_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_code"], name: "index_countries_on_country_code", unique: true
    t.index ["panel_provider_id"], name: "index_countries_on_panel_provider_id"
  end

  create_table "country_target_group_joins", force: :cascade do |t|
    t.bigint "country_id"
    t.bigint "target_group_id"
    t.index ["country_id"], name: "index_country_target_group_joins_on_country_id"
    t.index ["target_group_id"], name: "index_country_target_group_joins_on_target_group_id"
  end

  create_table "location_groups", force: :cascade do |t|
    t.citext "name", null: false
    t.bigint "panel_provider_id", null: false
    t.bigint "country_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_id"], name: "index_location_groups_on_country_id"
    t.index ["name"], name: "index_location_groups_on_name", unique: true
    t.index ["panel_provider_id", "country_id"], name: "index_location_groups_on_panel_provider_id_and_country_id"
    t.index ["panel_provider_id"], name: "index_location_groups_on_panel_provider_id"
  end

  create_table "location_groups_locations", id: false, force: :cascade do |t|
    t.bigint "location_id", null: false
    t.bigint "location_group_id", null: false
    t.index ["location_group_id", "location_id"], name: "index_location_groups_locations_on_location_group_and_location"
    t.index ["location_id", "location_group_id"], name: "index_location_groups_locations_on_location_and_location_group"
  end

  create_table "locations", force: :cascade do |t|
    t.citext "name", null: false
    t.string "external_id", null: false
    t.string "secret_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["external_id"], name: "index_locations_on_external_id", unique: true
    t.index ["name"], name: "index_locations_on_name", unique: true
  end

  create_table "panel_providers", force: :cascade do |t|
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "target_evaluations", force: :cascade do |t|
    t.float "price"
    t.string "external_id", null: false
    t.string "country_code", null: false
    t.string "target_group_id", null: false
    t.text "locations", array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["external_id"], name: "index_target_evaluations_on_external_id"
  end

  create_table "target_groups", force: :cascade do |t|
    t.citext "name", null: false
    t.string "external_id", null: false
    t.string "secret_code"
    t.bigint "panel_provider_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ancestry"
    t.index ["ancestry"], name: "index_target_groups_on_ancestry"
    t.index ["external_id"], name: "index_target_groups_on_external_id", unique: true
    t.index ["name"], name: "index_target_groups_on_name", unique: true
    t.index ["panel_provider_id"], name: "index_target_groups_on_panel_provider_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "country_target_group_joins", "countries"
  add_foreign_key "country_target_group_joins", "target_groups"
end
